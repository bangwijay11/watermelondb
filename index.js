/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite';
import schema from './src/app/localDb/models/schema';
import { Database } from '@nozbe/watermelondb';
import Library from './src/app/localDb/models/Library';

const adapter = new SQLiteAdapter({
  schema,
  // dbName: 'semongkoDB', // optional database name or file system path
  //   migrations, // optional migrations
  // synchronous: true, // synchronous mode only works on iOS. improves performance and reduces glitches in most cases, but also has some downsides - test with and without it
  //   experimentalUseJSI: true, // experimental JSI mode, use only if you're brave
});

export const database = new Database({
  adapter,
  modelClasses: [Library],
  actionsEnabled: true,
});

AppRegistry.registerComponent(appName, () => App);
