const Tables = {
  library: {
    name: 'library',
    columns: [
      { name: 'docId', type: 'number' },
      { name: 'name', type: 'string' },
      { name: 'type', type: 'string' },
      { name: 'extension', type: 'string' },
      { name: 'path', type: 'string' },
      { name: 'size', type: 'number' },
      { name: 'link', type: 'string' },
      { name: 'title', type: 'string' },
      { name: 'startDate', type: 'string' },
      { name: 'endDate', type: 'string' },
      { name: 'status', type: 'boolean' },
      { name: 'contentCount', type: 'number' },
      { name: 'openedAt', type: 'string' },
      { name: 'downloadedPath', type: 'string' },
      { name: 'docInsertedDate', type: 'string' },
      { name: 'docUpdatedDate', type: 'string' },
      { name: 'isDeleted', type: 'boolean' },
      { name: 'isGenerated', type: 'boolean' },
    ],
  },
  posts: {
    name: 'posts',
    columns: [
      { name: 'title', type: 'string' },
      { name: 'subtitle', type: 'string', isOptional: true },
      { name: 'body', type: 'string' },
      { name: 'is_pinned', type: 'boolean' },
      { name: 'date', type: 'string' },
      { name: 'created_at', type: 'number' },
      { name: 'updated_at', type: 'number' },
    ],
  },
};

export default Tables;
