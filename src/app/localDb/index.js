import {
  createTable,
  schemaMigrations,
} from '@nozbe/watermelondb/Schema/migrations';
import Migration from './migrations';

export default schemaMigrations({
  migrations: [
    {
      toVersion: 2,
      steps: [createTable(Migration.binaries)],
    },
  ],
});
