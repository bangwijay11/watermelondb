import { Model } from '@nozbe/watermelondb';
import { field } from '@nozbe/watermelondb/decorators';

export default class Library extends Model {
  static table = 'library';

  @field('docId') docId;

  @field('name') name;

  @field('type') type;

  @field('extension') extension;

  @field('path') path;

  @field('size') size;

  @field('link') link;

  @field('title') title;

  @field('startDate') startDate;

  @field('endDate') endDate;

  @field('status') status;

  @field('contentCount') contentCount;

  @field('openedAt') openedAt;

  @field('downloadedPath') downloadedPath;

  @field('docInsertedDate') docInsertedDate;

  @field('docUpdatedDate') docUpdatedDate;

  @field('isDeleted') isDeleted;

  @field('isGenerated') isGenerated;
}
