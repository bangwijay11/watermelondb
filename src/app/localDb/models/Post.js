import { Model } from '@nozbe/watermelondb';
import {
  children,
  date,
  field,
  readonly,
} from '@nozbe/watermelondb/decorators';

export default class Post extends Model {
  static table = 'posts';
  static associations = {
    comments: { type: 'has_many', foreignKey: 'post_id' },
  };

  @field('title') title;
  @field('body') body;
  @field('is_pinned') isPinned;
  @children('comments') comments;
  @readonly @date('created_at') createdAt;

  @readonly @date('updated_at') updatedAt;
}
