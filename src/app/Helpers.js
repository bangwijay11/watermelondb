export const LibrariesObject = (data, previous = []) => {
  data.forEach((doc) => {
    const extension = doc.name.split('.').slice(-1)[0];
    previous.push({
      docId: doc.id,
      name: doc.name,
      type: doc.type,
      extension: extension && extension === doc.name ? undefined : extension,
      path: doc.path,
      size: doc.size,
      link: doc.link,
      title: doc.title,
      startDate: doc.startDate,
      endDate: doc.endDate,
      status: doc.status,
      contentCount: (doc.content || []).filter(
        (x) => x.type !== 'folder' && x.status,
      ).length,
      docInsertedDate: doc.insertDate,
      docUpdatedDate: doc.lastEdited,
      isGenerated: doc.isGenerated,
    });
    if (doc.content && doc.content.length > 0) {
      LibrariesObject(doc.content, []).forEach((x) => previous.push(x));
    }
  });
  return previous;
};
